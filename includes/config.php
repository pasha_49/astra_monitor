<?php

//Можно создать conf.php в корне мониторинга, в который поместить любые из настроек, для более удобного обновления.
//При добавлении в него настроек, заменить "def" на "define".
if (file_exists("conf.php")) {
    include_once("conf.php");
}

function def($const,$value) {
    if (!defined($const)) {
        define($const, $value);
    }
}

////////////////////////////////////////////////////////////////////////////

    //Параметры для подключения к базе
    def('SQL_HOST','localhost');
    def('SQL_DB','astra');
    def('SQL_USER','user');
    def('SQL_PASSWORD','pas');

    //API_KEY для отправки уведомлений в pushbullet. Если больше одного, указывать через пробел. Раскомментировать для отправки (должен быть установлен php5-curl)
    //def('API_KEY','123123123qweqweqwe F.D78sadsdf98d7f');

    //Расскомментируйте для отправлки уведомлений в telegram. Настройки в includes/telegam/config.py
    //def('TELEGRAM','telegram true');


    //Внешний вид
    def('LEN_INPUT','25');//Длина строки "Вход" в таблице
    def('TIME_RESP','600');//Время, через которое изменится время канала на красное, если не приходит статистика (в секундах).
    def('LANG','ru');//Язык для выбора шаблона
    def('PAGE_RELOAD_TIME',60);//Время автообновления страниц, секунды

    //Названия для кастомизирования столбцов
    def('OPT_1','Измените');
    def('OPT_2','столбцы');
    def('OPT_3','через');
    def('OPT_4','конфиг');
    def('STR_1','Строка');

    //Поддержка udpxy(relay) для плеера
    //def('UDPXY_HOST','iptv2.server.my');
    //def('UDPXY_HOSTONSERVER',true);
    //def('UDPXY_PORT','4050');


    //Опции ниже не проверялись!!!

    //Раскомментируйте и впишите сервер, если хотите получать уведомления.
    //def('XMPPLOGIN','iptv@server.my'); // Имя пользователя
    def('XMPPPASS','iptvpass'); // пароль
    def('XMPPDOMAIN','server.my');   // имя домена
    def('XMPPHOST','xmpp.server.my');   //  сервер для подключения
    def('XMPPPORT','5222');   // порт
    def('XMPPALERTJID','your_jid@server.my'); // адрес назначения куда слать уведомления

    //Смс через через devinotele.com (в php необходима поддержка soap):
    //def('DEVINO_LOGIN','devinologin'); // логин для devinotele.com
    def('DEVINO_PASSWORD','devinopassword'); // пароль для devinotele.com
    def('DEVINO_FROM','IPTVMONITOR'); // Отправитель sms
    def('DEVINO_PHONE','+74957777777'); // Куда отправлять

////////////////////////////////////////////////////////////////////////////

    setlocale (LC_ALL,'ru_RU.UTF-8');

//-------System
    $path_parts = pathinfo($_SERVER['SCRIPT_FILENAME']);
    define('SCRIPT_DIR',$path_parts["dirname"]);
    $path_parts = pathinfo($_SERVER["PHP_SELF"]);

    $path_parts["dirname"]=='/'?'':$path_parts["dirname"];
    define('SCRIPT_WEBDIR',$path_parts["dirname"]);
    unset ($path_parts);

    $data=Array();
    read_request($data);

    function read_request(&$data) {
        foreach ($_REQUEST as $key => $val)
            {
            $data[$key] = rtrim(stripslashes($val));
            }
        $data['remote_ip'] = $_SERVER["REMOTE_ADDR"];
    }

?>